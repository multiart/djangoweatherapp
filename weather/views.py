from django.contrib import messages
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.urls import reverse
from django.urls import reverse_lazy
from django.views import generic


from .forms import CityForm
from .models import City
from .tasks import get_weather_data

# Create your views here.


class HomeView(generic.CreateView):
    template_name = 'home.html'
    model = City
    form_class = CityForm
    success_url = reverse_lazy('home')

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['cities'] = City.objects.order_by('name').all()
        return context

    def post(self, request, *args, **kwargs):
        result = super().post(request, *args, **kwargs)
        city = request.POST['name']
        if city:
            get_weather_data(city)
        return result

home_view = HomeView.as_view()


class UpdateView(generic.View):

    def get(self, request, *args, **kwargs):

        cities = City.objects.all()
        for city in cities:
            get_weather_data.delay(city.name)

        messages.add_message(request, messages.INFO,
                             'Weather update task started.')
        return HttpResponseRedirect(reverse('home'))

update_view = UpdateView.as_view()
