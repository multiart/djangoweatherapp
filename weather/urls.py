from django.urls import path

from . import views


urlpatterns = [
    path('', views.home_view, name='home'),
    path('update', views.update_view, name='update'),
]
