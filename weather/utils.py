import requests

from django.conf import settings

from .models import City


def fetch_data(city):
    api_key = settings.OPENWEATHER_API_KEY
    url = "{}{}&APPID={}".format(settings.OPENWEATHER_GET_CURRENT_WEATHER_URL, city, api_key)
    data = requests.get(url).json()

    city, created = City.objects.update_or_create(name=city, defaults={
        'temperature': data['main']['temp'],
        'description': data['weather'][0]['description'],
        'icon': data['weather'][0]['icon'],
        'country': data['sys']['country']
    })
