from .utils import fetch_data
from celery import task
from celery.task.schedules import crontab
from celery.decorators import periodic_task

from .models import City


@task
def get_weather_data(city):
    fetch_data(city)

@periodic_task(run_every=(crontab(minute='*/15')))
def get_all_weather_data():
    for city in City.objects.all():
        get_weather_data.delay(city.name)

