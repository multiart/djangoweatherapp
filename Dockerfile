FROM python:3.7-alpine as python_build

RUN mkdir -p /app
RUN pip install pipenv
ADD requirements.txt /

RUN pip install -r /requirements.txt

WORKDIR /app
ENV PYTHONUNBUFFERED 1



